DROP DATABASE IF EXISTS supermarket_inventory;
CREATE DATABASE IF NOT EXISTS supermarket_inventory;
USE supermarket_inventory;
set foreign_key_checks=0

CREATE TABLE product (id int(11) NOT NULL AUTO_INCREMENT,name varchar(100) NOT NULL,costprice int(11) NOT NULL,saleprice int(11) NOT NULL,stock int(11) NOT NULL,producttype int(11) NOT NULL,PRIMARY KEY (id),CONSTRAINT product_producttype_FK FOREIGN KEY (producttype) REFERENCES producttype (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `producttype` (`id` int(11) NOT NULL AUTO_INCREMENT,`initials` varchar(5) NOT NULL,`description` varchar(100) NOT NULL,PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO producttype VALUES (1, "LAC", "Lacteo");

INSERT INTO producttype VALUES (2, "ELE", "Electro");

INSERT INTO producttype VALUES (3, "COM", "Comestible");

INSERT INTO product VALUES (1, "Leche 1lt UAT Sancor", 30, 40, 50, 1);

INSERT INTO product VALUES (2, "Dulce de leche Vacalin", 35, 45, 50, 1);

INSERT INTO product VALUES (3, "iPhone X 256GB", 35000, 45000, 3, 2);

INSERT INTO product VALUES (4, "Samsung LED UHD 55", 25000, 30000, 10, 2);

INSERT INTO product VALUES (5, "Melitas", 20, 25, 100, 3);

INSERT INTO product VALUES (6, "Pringles", 40, 60, 20, 3);
