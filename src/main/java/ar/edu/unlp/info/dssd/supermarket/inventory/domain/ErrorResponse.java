package ar.edu.unlp.info.dssd.supermarket.inventory.domain;

public class ErrorResponse {

	public String message = "";
	
	public ErrorResponse(String message) {
		this.message = message;
	}
	
}