package ar.edu.unlp.info.dssd.supermarket.inventory.api;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import ar.edu.unlp.info.dssd.supermarket.inventory.domain.ErrorResponse;

@Provider
public class ApplicationExceptionMapper implements ExceptionMapper<WebApplicationException> {

	@Override
	public Response toResponse(WebApplicationException arg0) {
		return Response.status(arg0.getResponse().getStatus()).entity(new ErrorResponse(arg0.getMessage())).build();
	}

}
