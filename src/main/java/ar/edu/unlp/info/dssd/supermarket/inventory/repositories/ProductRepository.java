package ar.edu.unlp.info.dssd.supermarket.inventory.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unlp.info.dssd.supermarket.inventory.domain.Product;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {

	List<Product> findByNameIgnoreCaseContaining(String name);
	
}
