package ar.edu.unlp.info.dssd.supermarket.inventory.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import ar.edu.unlp.info.dssd.supermarket.inventory.api.Api;
import ar.edu.unlp.info.dssd.supermarket.inventory.api.ApplicationExceptionMapper;
import ar.edu.unlp.info.dssd.supermarket.inventory.api.CorsFilter;
import ar.edu.unlp.info.dssd.supermarket.inventory.api.GenericExceptionMapper;
import io.swagger.jaxrs.config.BeanConfig;

@Configuration
@PropertySource("classpath:application.properties")
public class JerseyConfig extends ResourceConfig {

	@Value("${api.version}")
	String apiVersion;

	public JerseyConfig() {
		registerEndpoints();
	}

	private void registerEndpoints() {
		// register(...);
		register(Api.class);
		register(GenericExceptionMapper.class);
		register(ApplicationExceptionMapper.class);
		register(CorsFilter.class);
		register(io.swagger.jaxrs.listing.ApiListingResource.class);
		register(io.swagger.jaxrs.listing.SwaggerSerializers.class);

		BeanConfig config = new BeanConfig();
		config.setTitle("Supermarket Inventory API");
		config.setVersion(apiVersion);
		config.setSchemes(new String[] { "http", "https" });
		config.setBasePath("/api");
		config.setResourcePackage(Api.class.getPackage().getName());
		config.setPrettyPrint(true);
		config.setScan(true);

	}
}
