package ar.edu.unlp.info.dssd.supermarket.inventory.api;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ar.edu.unlp.info.dssd.supermarket.inventory.domain.ErrorResponse;

@Provider
public class GenericExceptionMapper implements ExceptionMapper<Throwable> {

	Logger logger = LoggerFactory.getLogger(GenericExceptionMapper.class);

	public Response toResponse(Throwable ex) {
		logger.error("ERROR: " + ex.getMessage(), ex);
		return Response.status(500).entity(new ErrorResponse(ex.getMessage())).type(MediaType.APPLICATION_JSON).build();
	}
}
