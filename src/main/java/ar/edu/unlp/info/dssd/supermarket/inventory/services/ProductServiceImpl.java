package ar.edu.unlp.info.dssd.supermarket.inventory.services;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unlp.info.dssd.supermarket.inventory.domain.Product;
import ar.edu.unlp.info.dssd.supermarket.inventory.domain.ProductType;
import ar.edu.unlp.info.dssd.supermarket.inventory.repositories.ProductRepository;

@Service
public class ProductServiceImpl {

	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	ProductTypeServiceImpl productTypeService;

	@Transactional
	public Long create(Product aProduct) {
		ProductType findOne = null;
		
		if (aProduct.getProductType() != null && aProduct.getProductType().getId() != null)
			findOne = productTypeService.read(aProduct.getProductType().getId());
		
		if (findOne != null)
			aProduct.setProductType(findOne);
		
		productValidation(aProduct);

		return productRepository.save(aProduct).getId();
	}

	@Transactional(readOnly = true)
	public Product read(Long id) {
		Product findOne = productRepository.findOne(id);
		if (findOne == null) 
			throw new NotFoundException("Producto no encontrado");
		return 	productRepository.findOne(id);
	}

	@Transactional
	public Product update(Product aProduct) {
		Product findOne = productRepository.findOne(aProduct.getId());
		if (findOne == null) 
			throw new NotFoundException("Producto no encontrado");
		
		productValidation(aProduct);

		if (aProduct.getProductType() != null && aProduct.getProductType().getId() != null) {
			ProductType findOne2 = productTypeService.read(aProduct.getProductType().getId());
			findOne.setProductType(findOne2);
		}
		findOne.setCostPrice(aProduct.getCostPrice());
		findOne.setSalePrice(aProduct.getSalePrice());
		findOne.setName(aProduct.getName());
		findOne.setStock(aProduct.getStock());
		
		return findOne;
	}

	private void productValidation(Product aProduct) {
		if (aProduct.getStock() < 0)
			throw new BadRequestException("Stock negativo no está permitido");
		
		if (aProduct.getCostPrice() < 0)
			throw new BadRequestException("El costo del producto no puede ser negativo");
		
		if (aProduct.getSalePrice() < 0)
			throw new BadRequestException("El costo del producto no puede ser negativo");
				
	}

	@Transactional
	public void delete(Long id) {
		productRepository.delete(id);
	}
	
	@Transactional(readOnly = true)
	public Iterable<Product> findAll() {
		return productRepository.findAll();
	}
	
	@Transactional(readOnly = true)
	public Iterable<Product> findByName(String name) {
		return productRepository.findByNameIgnoreCaseContaining(name);
	}

}
