package ar.edu.unlp.info.dssd.supermarket.inventory.services;

import javax.ws.rs.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unlp.info.dssd.supermarket.inventory.domain.ProductType;
import ar.edu.unlp.info.dssd.supermarket.inventory.repositories.ProductTypeRepository;

@Service
@Transactional
public class ProductTypeServiceImpl {

	@Autowired
	ProductTypeRepository productTypeRepository;

	public ProductType read(Long id) {
		ProductType findOne = productTypeRepository.findOne(id);
		if (findOne == null)
			throw new NotFoundException("Product Type not found.");
		return findOne;
	}

	public Iterable<ProductType> findAll() {
		return productTypeRepository.findAll();
	}

	public Long create(ProductType productType) {
		return productTypeRepository.save(productType).getId();
	}
	
	public void delete(Long id) {
		productTypeRepository.delete(id);
	}

}
