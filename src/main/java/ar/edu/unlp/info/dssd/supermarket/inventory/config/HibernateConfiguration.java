package ar.edu.unlp.info.dssd.supermarket.inventory.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import ar.edu.unlp.info.dssd.supermarket.inventory.Application;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
public class HibernateConfiguration {

	Logger logger = LoggerFactory.getLogger(HibernateConfiguration.class);
	
	@Value("${database.url}")
	private String url = "jdbc:mysql://127.0.0.1:3306";

	@Value("${database.user}")
	private String user = "root";

	@Value("${database.password}")
	private String password = "1A2B3C4E";

	@Value("${database.hbm2ddl}")
	private String hbm2ddl = "create";

	@Value("${database.schema}")
	private String schema = "supermarket_inventory";

	private String autoreconnect = "?autoReconnect=true";

	public DataSourceBuilder getDataSourceBuilder() {
		return DataSourceBuilder.create().url(url + "/" + schema + autoreconnect).username(user).password(password)
				.driverClassName("com.mysql.jdbc.Driver");
	}

	private HibernateJpaVendorAdapter vendorAdaptor() {
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setShowSql(true);
		return vendorAdapter;
	}

	@Bean
	@Primary
	@ConfigurationProperties(prefix = "spring.datasource.tomcat")
	public DataSource dataSource() {
		return this.logAndReturn(this.getDataSourceBuilder().build());
	}

	private DataSource logAndReturn(DataSource ds) {
		logger.warn("-----##### DataSource class: " + ds);
		return ds;
	}

	@Bean(name = "transactionManager")
	public JpaTransactionManager jpaTransactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return transactionManager;
	}

	@Bean(name = "entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactoryBean.setJpaVendorAdapter(vendorAdaptor());
		entityManagerFactoryBean.setDataSource(dataSource());
		entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistenceProvider.class);
		entityManagerFactoryBean.setJpaProperties(jpaHibernateProperties());
		entityManagerFactoryBean.setPackagesToScan(Application.DOMAIN_PACKAGE);
		entityManagerFactoryBean.setPersistenceUnitName("entityManagerFactory");
		return entityManagerFactoryBean;
	}

	private Properties jpaHibernateProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.hbm2ddl.auto", hbm2ddl);
		properties.setProperty("hibernate.default_schema", schema);
		return properties;
	}
}
