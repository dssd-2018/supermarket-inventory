package ar.edu.unlp.info.dssd.supermarket.inventory.services;

import javax.ws.rs.ServerErrorException;
import javax.ws.rs.WebApplicationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

@Service
public class AuthServiceImpl {

	Logger logger = LoggerFactory.getLogger(AuthServiceImpl.class);

	public void auth(String bearer) {
		try {
			HttpResponse<String> asString = Unirest.post("http://auth.supermarket-dssd.ml/auth/authenticate")
					.header("Authorization", bearer).asString();
			if (asString.getStatus() != 200) {
				throw new WebApplicationException(asString.getStatus());
			}
		} catch (UnirestException e) {
			logger.error(e.getMessage(), e);
			throw new ServerErrorException(500);
		}
	}

}
