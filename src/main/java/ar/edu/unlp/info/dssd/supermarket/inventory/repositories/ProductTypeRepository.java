package ar.edu.unlp.info.dssd.supermarket.inventory.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unlp.info.dssd.supermarket.inventory.domain.ProductType;

@Repository
public interface ProductTypeRepository extends PagingAndSortingRepository<ProductType, Long> {

}
