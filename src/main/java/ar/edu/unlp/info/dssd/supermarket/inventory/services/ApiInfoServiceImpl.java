package ar.edu.unlp.info.dssd.supermarket.inventory.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unlp.info.dssd.supermarket.inventory.domain.ApiInfo;
import ar.edu.unlp.info.dssd.supermarket.inventory.repositories.ProductTypeRepository;

@Service
@PropertySource("classpath:application.properties")
public class ApiInfoServiceImpl {

	@Value("${api.version}")
	String apiVersion = "0";

	@Autowired
	ProductTypeRepository productTypeRepository;

	@Transactional
	public ApiInfo getInfo() {
		ApiInfo info = new ApiInfo();

		info.version = apiVersion;
		info.sha = System.getenv("COMMIT_SHA");
		info.deploy_timestamp = System.getenv("LAST_DEPLOY_TIMESTAMP");
		info.status = this.testDatabaseStatus();
		return info;
	}

	private String testDatabaseStatus() {
		productTypeRepository.count();
		return "ok";
	}

}
