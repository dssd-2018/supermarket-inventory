package ar.edu.unlp.info.dssd.supermarket.inventory.api;

import java.net.URI;
import java.net.URISyntaxException;

import javax.inject.Singleton;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import ar.edu.unlp.info.dssd.supermarket.inventory.domain.Product;
import ar.edu.unlp.info.dssd.supermarket.inventory.domain.ProductType;
import ar.edu.unlp.info.dssd.supermarket.inventory.services.ApiInfoServiceImpl;
import ar.edu.unlp.info.dssd.supermarket.inventory.services.AuthServiceImpl;
import ar.edu.unlp.info.dssd.supermarket.inventory.services.ProductServiceImpl;
import ar.edu.unlp.info.dssd.supermarket.inventory.services.ProductTypeServiceImpl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Component
@Singleton
@Path("/")
@io.swagger.annotations.Api
public class Api {

	Logger logger = LoggerFactory.getLogger(Api.class);

	@Autowired
	ProductServiceImpl productService;
	@Autowired
	ProductTypeServiceImpl productTypeService;
	@Autowired
	ApiInfoServiceImpl apiInfo;
	@Autowired
	AuthServiceImpl authService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Api Status", tags = { "info" })
	public Response ping() {
		try {
			return Response.ok(apiInfo.getInfo()).build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getMessage()).build();
		}
	}

	@GET
	@Path("product")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Listar productos", tags = { "Producto" })
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = Product.class, responseContainer = "List", message = "Productos recuperados."), })
	public Response products(@QueryParam("name") @ApiParam("name") String name, @Context HttpHeaders headers) {
		this.checkAuth(headers);
		if (name == null)
			return Response.ok(productService.findAll()).build();
		return Response.ok(productService.findByName(name)).build();
	}

	@GET
	@Path("product/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Buscar producto", tags = { "Producto" })
	@ApiResponses(value = { @ApiResponse(code = 200, response = Product.class, message = "Productos recuperados"),
			@ApiResponse(code = 404, response = String.class, message = "Producto no encontrado") })
	public Response products(@PathParam("id") long id, @Context HttpHeaders headers) {
		this.debug(id);
		this.checkAuth(headers);
		return Response.ok(productService.read(id)).build();
	}

	private void debug(Object... objects) {
		logger.info(new Gson().toJson(objects));
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("product")
	@ApiOperation(value = "Crear producto", tags = { "Producto" })
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Producto guardado."),
			@ApiResponse(code = 409, response = String.class, message = "Datos del producto incorrectos.") })
	public Response create(Product aProduct, @Context HttpHeaders headers) throws URISyntaxException {
		this.debug(aProduct);
		this.checkAuth(headers);
		return Response.created(new URI("product/" + productService.create(aProduct).toString())).build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("product")
	@ApiOperation(value = "Actualizar producto", tags = { "Producto" })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Producto modificado."),
			@ApiResponse(code = 409, response = String.class, message = "Datos del producto incorrectos."),
			@ApiResponse(code = 404, response = String.class, message = "Producto no encontrado.") })
	public Response update(Product aProduct, @Context HttpHeaders headers) {
		this.debug(aProduct);
		this.checkAuth(headers);
		return Response.ok(productService.update(aProduct)).build();
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("producttype")
	@ApiOperation(value = "Listar tipos de producto", tags = { "TipoProducto" })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
	public Response productsType(@Context HttpHeaders headers) {
		this.checkAuth(headers);
		return Response.ok(productTypeService.findAll()).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("producttype")
	@ApiOperation(value = "Crear tipo de producto", tags = { "TipoProducto" })
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Creado") })
	public Response create(ProductType productType, @Context HttpHeaders headers) throws URISyntaxException {
		this.debug(productType);
		this.checkAuth(headers);
		return Response.created(new URI("producttype/" + productTypeService.create(productType).toString()))
				.build();
	}

	@DELETE
	@Path("product/{id}")
	@ApiOperation(value = "Borrar producto", tags = { "Producto" })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Borrado") })
	public Response delete(@PathParam("id") Long id, @Context HttpHeaders headers) {
		this.debug(id);
		this.checkAuth(headers);
		productService.delete(id);
		return Response.ok().build();
	}

	@DELETE
	@Path("producttype/{id}")
	@ApiOperation(value = "Borrar tipo de producto", tags = { "TipoProducto" })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Borrado"),
			@ApiResponse(code = 409, message = "Tipo de producto en uso") })
	public Response deleteproducttype(@PathParam("id") Long id, @Context HttpHeaders headers) {
		this.debug(id);
		this.checkAuth(headers);
		productTypeService.delete(id);
		return Response.ok().build();
	}

	private void checkAuth(HttpHeaders headers) {
		String headerString = headers.getHeaderString("Authorization");
		if (headerString == null || headerString.isEmpty())
			throw new BadRequestException("Token is missing");

		authService.auth(headerString);
	}

}
