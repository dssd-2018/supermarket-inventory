package ar.edu.unlp.info.dssd.supermarket.inventory;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import ar.edu.unlp.info.dssd.supermarket.inventory.api.Api;
import ar.edu.unlp.info.dssd.supermarket.inventory.domain.Product;
import ar.edu.unlp.info.dssd.supermarket.inventory.domain.ProductType;
import ar.edu.unlp.info.dssd.supermarket.inventory.services.ProductServiceImpl;
import ar.edu.unlp.info.dssd.supermarket.inventory.services.ProductTypeServiceImpl;
import org.junit.*;

public class ProductTest extends IntegrationTest {

	@Autowired
	ProductTypeServiceImpl productTypeService;

	@Autowired
	ProductServiceImpl productService;

	@Test
	public void testCreate() {
		ProductType aProductType = new ProductType();
		aProductType.setDescription("Alimentos");
		aProductType.setInitials("ALI");

		productTypeService.create(aProductType);

		Product aProduct = new Product();

		aProduct.setProductType(aProductType);
		aProduct.setCostPrice(45);
		aProduct.setSalePrice(70);

		aProduct.setName("Leche SanCor 1lt sachet");

		productService.create(aProduct);

		Assert.assertTrue(aProduct.getId() != null);
	}
	
	@Test
	public void testUpdate() {
		this.testCreate();
		
		Iterable<Product> findAll = productService.findAll();
		
		for (Product aProduct : findAll) {
			aProduct.setName("CarlosVaca");
			productService.update(aProduct);
		}
		
	}

}
