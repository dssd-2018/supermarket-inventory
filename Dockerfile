FROM maven:alpine as maven

COPY pom.xml .

RUN ["mvn", "verify", "clean", "--fail-never"]

COPY . .

RUN ["mvn", "package", "-DskipTests"]

ENTRYPOINT ["java", "-jar", "target/supermarket-inventory.jar"]
